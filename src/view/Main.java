package view;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

import models.Peca;
import models.Revisao;


public class Main {
	public static void teste() {
		int dataProximaRevisao[]= {0,0,0};
		int dataAtual[]= {0,0,0};
		boolean revisar=false;

		Scanner tec = new Scanner(System.in);

		System.out.println("Digite a data marcada para Proxima revisao");
		String data=tec.next();
		String textoSeparado[] = data.split("/");

		for (int i = 0; i < 3; i++) {
			dataProximaRevisao[i]=Integer.parseInt(textoSeparado[i]); 
		}
		//informando a data
		Calendar user = new GregorianCalendar(dataProximaRevisao[2], dataProximaRevisao[1], dataProximaRevisao[0]);
		//Intsnciando calendario gregoriano
		Calendar now = new GregorianCalendar();
		//retorna true se a data informada acima for anterior a data atual
		System.out.println(user.after(now));
	}
	
	public static void main(String[] args) {
		Peca p1 = new Peca("Sistema de freio", 1000.0);
		Peca p2 = new Peca("oleo", 1000.0);
		Peca p3 = new Peca("farol", 6000.0);
		
		Revisao revisao = new Revisao(1500.0);
		revisao.getPecas().add(p1);
		revisao.getPecas().add(p2);
		revisao.getPecas().add(p3);
		
		revisao.verificaPecas();
		revisao.realizarRevisao();
		
		System.out.println(revisao.getPecas().toString());

	}
}
