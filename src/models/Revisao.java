package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Revisao {
	private List <Peca> pecas;
	private Double kmAtual;
	private int dataAtual[]= {0,0,0};
	private boolean revisar=false;
	
	public List<Peca> getPecas() {
		return pecas;
	}

	public void setPecas(List<Peca> pecas) {
		this.pecas = pecas;
	}

	public Double getKmAtual() {
		return kmAtual;
	}

	public void setKmAtual(Double kmAtual) {
		this.kmAtual = kmAtual;
	}

	public int[] getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(int[] dataAtual) {
		this.dataAtual = dataAtual;
	}

	public boolean isRevisar() {
		return revisar;
	}

	public void setRevisar(boolean revisar) {
		this.revisar = revisar;
	}

	public Revisao(Double kmAtual) {
		this.pecas = new ArrayList <Peca>();
		this.kmAtual = kmAtual;
	}
	
	public void verificaPecas() {
		for (int i = 0; i < pecas.size(); i++) {
			pecas.get(i).setKmUltimaRevisao(kmAtual);
			if (pecas.get(i).getKmParaRevisao()<kmAtual) {
				pecas.get(i).setNecessarioRevisar(true);
			}
			System.out.println(pecas.get(i).getNome()+" "+pecas.get(i).isNecessarioRevisar());
		}
	}
	
	public void realizarRevisao() {
		Scanner tec = new Scanner(System.in);
		System.out.println("Digite da revisao");
		String data=tec.next();
		String textoSeparado[] = data.split("/");

		for (int i = 0; i < 3; i++) {
			dataAtual[i]=Integer.parseInt(textoSeparado[i]); 
		}
		for (Peca p: pecas) {
			p.setDataUltimaRevisao(dataAtual);
			p.setNecessarioRevisar(false);
			System.out.println(p.getNome()+" "+p.getDataUltimaRevisao().toString()+" "+p.isNecessarioRevisar());
		}
		
		
		
	}
	
	
}
