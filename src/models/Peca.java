package models;

import java.util.Arrays;

public class Peca {
	private String nome="";
	private Double kmParaRevisao=0.0;
	private Double kmUltimaRevisao=0.0;
	private int DataProximaRevisao[]= {0,0,0};
	private int DataUltimaRevisao[]= {0,0,0};
	private boolean necessarioRevisar=false;
	
	public int[] getDataUltimaRevisao() {
		return DataUltimaRevisao;
	}

	public void setDataUltimaRevisao(int[] dataUltimaRevisao) {
		DataUltimaRevisao = dataUltimaRevisao.clone();
	}

	public boolean isNecessarioRevisar() {
		return necessarioRevisar;
	}

	public void setNecessarioRevisar(boolean necessarioRevisar) {
		this.necessarioRevisar = necessarioRevisar;
	}

	public Peca(String nome, Double kmParaRevisao) {
		super();
		this.nome = nome;
		this.kmParaRevisao = kmParaRevisao;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getKmUltimaRevisao() {
		return kmUltimaRevisao;
	}

	public void setKmUltimaRevisao(Double kmUltimaRevisao) {
		this.kmUltimaRevisao = kmUltimaRevisao;
	}

	//Getters and setters
	public void setDataProximaRevisao(int dataProximaRevisao[]) {
		DataProximaRevisao = dataProximaRevisao;
	}

	public int[] getDataProximaRevisao() {
		return DataProximaRevisao;
	}

	public Double getKmParaRevisao() {
		return kmParaRevisao;
	}

	public void setKmParaRevisao(Double kmParaRevisao) {
		this.kmParaRevisao = kmParaRevisao;
	}

	@Override
	public String toString() {
		return "Nome: " + nome + " \n Quilometragem para revisao: " + kmParaRevisao + "\n Quimotragem da ultima revisao: " + kmUltimaRevisao
				+ "\n Data Proxima Revisao: " + Arrays.toString(DataProximaRevisao) + "\n Data Ultima Revisao: "
				+ Arrays.toString(DataUltimaRevisao) + "\n Necessario Revisar:" + necessarioRevisar + "\n\n";
	}
	
	
	
	
	
}
